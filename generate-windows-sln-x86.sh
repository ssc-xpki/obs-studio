#!/usr/bin/env bash
export BASE_DIR=c:\obs-studio
export WINDOWS_DEPENDENCIES_DIR="${BASE_DIR}\windows_dependencies"
export TIMESTAMP=`date +%s%N | cut -b1-13`
cmake -DRELEASE_CANDIDATE="25.0.8-rc1.${TIMESTAMP}.[Comcast Hardened]" -DDepsPath:STRING="c:\obs-studio\windows_dependencies\dependencies2017\win32" -DQTDIR="c:\obs-studio\windows_dependencies\Qt_5.10.1\5.10.1\msvc2017" -DCMAKE_CONFIGURATION_TYPES:STRING="Debug;Release;MinSizeRel;RelWithDebInfo" ..
