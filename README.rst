This is a custom build of OBS (obsproject.com).  It's based off of version 25.0.8.

This version is different from the open source version.  It -

1. Only supports rtsps:// (secure) and a custom streaming provider.
2. Disables automatic software updates.
3. Upgrades the versions of mbedTLS (2.16.6) and cURL (7.77.0).

The `windows_dependencies` directory contains development packages of FFmpeg, x264, cURL, and mbedTLS.  It was based off of the pre-built windows dependencies for VS2017 that was found here:
`VS2017: https://obsproject.com/downloads/dependencies2017.zip`.  It was slightly modified to include a newer version of mbedTLS (2.16.6) and libCurl (7.77.0).

To build, make a `build` directory.  Then `cd build` and run `../generate-windows-sln-x86.sh` to generate the Visual Studio 2017 .sln file.  Then, open up that file and build from Studio.
 