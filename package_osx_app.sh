#!/bin/sh
export OBS_APP=./OBS.app
export BUILD=./build
cp ./build_app.py ${BUILD}
chmod 755 ${BUILD}/package_osx_app.py
cd ${BUILD}
sudo rm -Rf ${OBS_APP}
sudo python package_osx_app.py -d ./rundir/RelWithDebInfo
sudo rm -Rf ${OBS_APP}/Contents/Resources/bin 
sudo rm -Rf ${OBS_APP}/Contents/Resources/data
sudo rm -Rf ${OBS_APP}/Contents/Resources/obs-plugins 
sudo cp -r ./rundir/RelWithDebInfo/* ${OBS_APP}/Contents/Resources
cd ..
